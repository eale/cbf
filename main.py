import math
import re
from math import log


class BloomFilter:
    def __init__(self, text: str):
        self.percent_of_fail = 0.1

        self.text = re.sub(r'[^\w\s]','', text).lower()

        text_length = len(self.text.split())
        self.list_length = int(text_length * (-log(self.percent_of_fail)/log(2)**2))

        self.list_with_bits = [0 for i in range(self.list_length)]
        self.count_of_hashes = round((self.list_length / text_length) * math.log(2))

        self.add(self.text)

    def remove(self, text_to_remove):
        for element in text_to_remove.split():
            for i in range(self.count_of_hashes):
                bit_id = self._hash(element, i)
                self.list_with_bits[bit_id] -= 1

    def _hash_djb2(self, s):
        hash_result = 5381
        for x in s:
            hash_result = ((hash_result << 5) + hash_result) + ord(x)
        return hash_result % self.list_length

    def _hash(self, item, number_of_hash):
        return self._hash_djb2(str(number_of_hash) + item)

    def add(self, text_to_add):
        for element in text_to_add.split():
            for i in range(self.count_of_hashes):
                bit_id = self._hash(element, i)
                self.list_with_bits[bit_id] += 1

    def check(self, word):
        for i in range(self.count_of_hashes):
            bit_id = self._hash(word, i)
            if not self.list_with_bits[bit_id]:
                return False

        return True


def main():
    with open("text.txt") as file:
        text = file.read()

    bloom_filter = BloomFilter(text)

    while True:
        word = input("Input word for check \n")
        print(bloom_filter.check(word))


if __name__ == '__main__':
    main()
